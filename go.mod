module gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/v2

require (
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.5.6
	gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan v1.2.0
)

go 1.13
