package plugin

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

var JsExtensions = []string{
	".js",
	// ".html", ".mustache", ".hbs", ".hdbs", ".ejs", ".dust", ".json",
	// ".tl", ".tpl", ".tmpl", ".pug", ".ect", ".sh", ".yml",
}

func Match(path string, info os.FileInfo) (bool, error) {
	ext := filepath.Ext(info.Name())
	if ext == "" {
		return false, nil
	}
	for _, jsExt := range JsExtensions {
		if ext == jsExt {
			return true, nil
		}
	}
	return false, nil
}

func init() {
	plugin.Register("nodejs-scan", Match)
}
