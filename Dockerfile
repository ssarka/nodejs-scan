FROM  node:9.11.2-alpine

ARG NODEJS_SCAN_VERSION
ARG RULES_SHA256SUM=f3778c2e724ead32c673864577031598582471dc0ea29b6e655fa4e6c727b799

# Please read the "Updating the underlying Scanner" section in
# the README before bumping to a newer version
ENV NODEJS_SCAN_VERSION=${NODEJS_SCAN_VERSION:-3.3} \
    NODE_PATH=/home/node/node_modules

USER node
WORKDIR /home/node
COPY --chown=node:node babel.config.json .
COPY --chown=node:node package.json .
COPY --chown=node:node yarn.lock .

RUN yarn --frozen-lockfile && yarn cache clean

ADD --chown=node:node https://raw.githubusercontent.com/ajinabraham/NodeJsScan/v${NODEJS_SCAN_VERSION}/core/rules.xml .

RUN echo "$RULES_SHA256SUM  rules.xml" | sha256sum -c

COPY --chown=root:root analyzer /

ENTRYPOINT []
CMD ["/analyzer", "run"]
